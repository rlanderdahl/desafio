## Intro

### Why Clojurescript (and Reagent/Re-frame)?

Clojurescript is a version of the Clojure programming language that compiles to efficient Javascript. It is a [well designed functional](https://clojure.org/about/state), dynamic language which has a primary goal of building [complex systems](https://clojure.org/about/rationale) in a robust but simple way.


In this project we're using a small library called Reagent, which is an amazingly simple interface to Facebook's React that, unlike React itself, requires learning only a handful of concepts. Reagent will be even faster than plain React most of the time, thanks to optimizations made possible by ClojureScript. To manage state we're using Re-frame.


[Functional languages are ideal for writing large applications because they eschew global state and favor immutability as the default, allowing for components that are testable and reusable by nature](https://yogthos.github.io/ClojureDistilled.html). Clojurescript, being a functional language, also allows us to type less, since functionality provided by JS libraries like Immutable.JS are built into the language (and, of course, we have macros at our disposal, [lisp's secret weapon](http://www.paulgraham.com/avg.html)). We also have all the tools we'll ever need to manage immutable data structures (cursors, transducers, zippers) and efficient mutability (atoms and transients). 


The immutability of ClojureScript also enables efficient DOM diffing. React itself is very fast and Reagent, our small wrapper around React, is capable of being even faster because of optimizations done in the language. After creating the virtual DOM described by your components, React does a diff on it, which is based on a critical function: shouldComponentUpdate. When this returns false, React will not compute any children of the component. By default, the shouldComponentUpdate implementation is extremely conservative because JavaScript code tends to mutate objects and arrays. But Reagent leverages ClojureScript's default immutability for faster comparison in shouldComponentUpdate, which is why it is faster than vanilla React. It all boils down to this: immutable data= fast reference equality checks; mutable data= slow, deep equality checks.


On top of all this, ClojureScript capitalizes on Google Closure (an unfortunate naming coincidence), a library that does dead code removal and minification. It is an integral part of the compilatiom process and, with it, build time is significantly lower, even with advanced optimizations, when compared to Webpack.


Note: if you’re familiar with React, when you read this source code you might wonder where componentDidMount, createClass, props, render and all of that other clutter went. Welcome to Reagent where components are just regular functions that return plain data, state is kept in closures and there’s no boilerplate at all!

 
## Pre-requisites

* Python 2/3
* Postgresql
* Npm
* Java


## Installation

1) Inside "desafio/"
```text
pip install -r requirements.txt
npm install
```
2) Then, navigate to "desafio/client/desafio". This is the Clojurescript client.
```text
lein figwheel
```

3) You're all set. Now go to http://localhost:8000/ and you should see it running.

## Project Structure

* client/ - All Clojurescript code goes here.
* config/ - General Django settings.
* desafio/ - Django apps, templates and static assets.
* desafio/static/scss/ - CSS source.
* requirements/ - Home of local, production and test requirements.

```text
├── Procfile
├── README.md
├── client
│   └── desafio
├── config
│   └── settings
├── desafio
│   ├── contrib
│   │   └── sites
│   ├── core
│   ├── media
│   ├── static
│   │   ├── css
│   │   ├── fonts
│   │   ├── img
│   │   ├── js
│   │   └── scss
│   ├── templates
│   └── users
├── env.example
├── manage.py
├── package.json
├── requirements
├── requirements.txt
├── runtime.txt
└── setup.cfg
```
