# -*- coding: utf-8 -*-

import factory  
import factory.django
from factory.faker import Faker

from desafio.users.models import User
from .models import Article, Subject


class ArticleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Article

    author   = factory.Iterator(User.objects.all().exclude(id=1))
    hero_img = Faker('image_url', width=555, height=354)
    subject  = factory.Iterator(Subject.objects.all())
    text     = Faker('text', max_nb_chars=500)
    title    = Faker('catch_phrase')

class SubjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Subject

    name = Faker('sentence', nb_words=1)
