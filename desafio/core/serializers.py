from rest_framework import serializers
from .models import Article, Subject
from desafio.users.serializers import UserSerializer


class ArticleSerializer(serializers.ModelSerializer):
	author = UserSerializer()
	class Meta:
		model = Article
		fields = '__all__'
		depth = 2

class SubjectSerializer(serializers.ModelSerializer):
	class Meta:
		model = Subject
		fields = '__all__'
