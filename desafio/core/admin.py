from django.contrib import admin
from .models import Article, Subject


class ArticleAdmin(admin.ModelAdmin):
	date_hierarchy = 'created'
	list_display   = ('id', 'title', 'subject', 'hero_img', 'created', 'modified')
	list_filter    = ('subject', 'author')
	search_fields  = ('id', 'title', 'subject', 'text')

admin.site.register(Article, ArticleAdmin)
admin.site.register(Subject)
