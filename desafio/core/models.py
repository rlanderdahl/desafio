# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from autoslug import AutoSlugField
from model_utils.models import TimeStampedModel


class Article(TimeStampedModel):
    author   = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    hero_img = models.URLField(_("Hero Image"))
    slug     = AutoSlugField(_("Slug"), populate_from='title')
    subject  = models.ForeignKey('Subject', on_delete=models.CASCADE)
    text     = models.TextField(_("Text"))
    title    = models.CharField(_("Title"), max_length=255)

    def __unicode__(self):
        return self.title 


class Subject(TimeStampedModel):
    name  = models.CharField(_("Name"), max_length=255)
    color = models.CharField(_("Default Color"), default="#686868", max_length=255)

    def __unicode__(self):
        return self.name
