from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'desafio.users'
    verbose_name = "Users"

    def ready(self):
      from . import signals
