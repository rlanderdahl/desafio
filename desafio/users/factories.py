# -*- coding: utf-8 -*-

import factory  
import factory.django
from faker import Factory as FakerFactory

from .models import User


faker = FakerFactory.create()


class UserFactory(factory.django.DjangoModelFactory):
	class Meta:
		model = User
		django_get_or_create = ('username',)

	email    = factory.LazyAttribute(lambda obj: obj.username + "@example.com")
	name     = factory.Faker('name')
	picture  = factory.LazyAttribute(lambda o: faker.image_url(width=50, height=50))
	username = factory.Sequence(lambda n: 'user_%d' % n)
