(ns desafio.views
    (:require [re-frame.core :refer [dispatch subscribe]]
              [free-form.core :as ff]
              [desafio.components :as c]
              [desafio.util :refer [highlight on-enter logged-in?]]))


;; -- Panels -----------------------------------------------------------------

;; home page

(defn home-panel []
  (let [articles (subscribe [:filtered-articles])
        subjects (subscribe [:subjects])]
    [:div {:id "app"}
      [c/header @subjects]
      [:div.f-container
        [c/render-highlighted @articles]
        [:hr.sep.hidden-md-down]
        [c/render-remaining @articles]]]))

(defn subject-panel []
  (let [subjects   (subscribe [:subjects])
        subject-id (subscribe [:subject-id])
        subject-id (js/parseInt @subject-id)
        articles   (subscribe [:articles])
        articles   (filter #(#{subject-id} (get-in % [:subject :id])) @articles)]
    [:div
      [c/header @subjects]
      (for [article articles]
        [c/single-article article "grid__item sm-w-1/2 md-w-1/3"])]))

;; user login

(defn login-panel []
  (let [values   (subscribe [:login-data])
        errors   (subscribe [:login-errors])
        subjects (subscribe [:subjects])]
    (fn []
      [:div
        [c/header @subjects]
        [:div.f-container
          [:div.login.vh-align
            [:h1.title "User Area"]
            [ff/form @values @errors (fn [keys value] (dispatch [:login-set-attr keys value]))
              [:div
                [:div.form-group
                  [:div.errors {:free-form/error-message {:key :general}}]
                  [:label.field-label "Username"]
                  [:input {:free-form/input       {:key :username}
                           :free-form/error-class {:key :username :error "error"}
                           :type                  :text
                           :on-key-press          (on-enter #(dispatch [:login-submit]))}]]
                [:div.form-group
                  [:label.field-label "Password"]
                  [:input {:free-form/input       {:key :password}
                             :free-form/error-class {:key :password :error "error"}
                             :type                  :password
                             :on-key-press          (on-enter #(dispatch [:login-submit]))}]]
                [:div.form-group
                  [:button.btn.btn--primary {:on-click #(dispatch [:login-submit])}
                    "Login"]]]]]]])))

;; user settings dashboard

(defn me-panel []
  (let [user         (subscribe [:login-data])
        subjects     (subscribe [:subjects])
        sel-subjects (subscribe [:sel-subjects])]
    (fn []
      [:div
        [c/header @subjects]
        [:div.f-container
          [:div.interests.vh-align
            [:h1.title "Welcome, "
              [:span.username (@user :username)]]
            [:p "My Interests"]
            [c/select-subjects @subjects @sel-subjects]]]])))

;; main

(defn- panels [panel-name]
  (case panel-name
    :home-panel [home-panel]
    :me-panel [me-panel]
    :login-panel [login-panel]
    :subject-panel [subject-panel]
    [:div]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (subscribe [:active-panel])]
    (fn []
      [show-panel @active-panel])))
