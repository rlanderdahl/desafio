(ns desafio.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :refer [reg-sub]]))

;; -- Subscriptions ----------------------------------------------------------

;; A subscription handler is a function which is re-run when its input signals
;; change. Each time it is rerun, it produces a new output (return value).

(reg-sub
  :active-panel
  (fn [db _]
    (:active-panel db)))

(reg-sub
  :subjects
   (fn [db _]
    (:subjects db)))

(reg-sub
  :articles
    (fn [db _]
      (:articles db)))

(reg-sub
  :token
  (fn [db _]
    (:token db)))

(reg-sub
  :svd-subjects
  (fn [db _]
    (:svd-subjects db)))

(reg-sub
  :sel-subjects
  (fn [db _]
    (:sel-subjects db)))

(reg-sub
  :login-data
  (fn [db _]
    (:login-data db)))

(reg-sub
  :login-errors
  (fn [db _]
    (:login-errors db)))

(reg-sub
  :menu-open?
  (fn [db _]
    (:menu-open? db)))

(reg-sub
  :subject-id
  (fn [db _]
    (:subject-id db)))

;; -- Computed ----------------------------------------------------------

(reg-sub
  :filtered-articles
  :<- [:articles]
  :<- [:svd-subjects]
  (fn [[articles filters]]
    (if (or (nil? filters) (empty? filters))
      articles
      (filter #((set filters) (% :subject)) articles))))
