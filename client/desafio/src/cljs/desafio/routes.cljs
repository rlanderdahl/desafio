(ns desafio.routes
    (:require-macros [secretary.core :refer [defroute]])
    (:import goog.History)
    (:require [secretary.core :as secretary]
              [accountant.core :as accountant]
              [goog.events :as events]
              [goog.history.EventType :as EventType]
              [re-frame.core :refer [dispatch subscribe]]
              [desafio.util :refer [logged-in?]]))

;; -- Helpers ----------------------------------------------------------

(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true))
  (accountant/configure-navigation!
    {:nav-handler
     (fn [path]
       (secretary/dispatch! path))
     :path-exists?
     (fn [path]
       (secretary/locate-route path))})
  (accountant/dispatch-current!))   

;; -- Routes ----------------------------------------------------------

(defn app-routes []
  (defroute "/" [] (dispatch [:set-active-panel :home-panel]))
  (defroute "/login" [] 
    (if (logged-in?)
      (accountant/navigate! "/me")
      (dispatch [:set-active-panel :login-panel])))
  (defroute "/me" []
    (if (logged-in?)
      (dispatch [:load-me-panel])
      (accountant/navigate! "/login")))
  (defroute "/subject/:id" [id]
      (dispatch [:set-active-panel :subject-panel])
      (dispatch [:set-subject-id id]))

  ;; --------------------
  (hook-browser-navigation!))
