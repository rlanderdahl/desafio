(ns desafio.components
  (:require [re-frame.core :refer [dispatch subscribe]]
            [desafio.util :refer [highlight on-enter logged-in?]]))

;; -- Components ----------------------------------------------------------

(defn hamburger-menu [toggle-cls menu-open?]
    [:div.hidden-md-up
      [:button {:class (str "hamburger hamburger--collapse " toggle-cls) 
                :type "button"
                :on-click #(dispatch [:toggle-menu (not menu-open?)])}
          [:span.hamburger-box
            [:span.hamburger-inner]]]])

(defn header [subjects]
  (let [menu-open? (subscribe [:menu-open?])
        toggle-cls (if @menu-open? "is-active" "")]
    [:header.menu
      [:div.f-container.clearfix
        [hamburger-menu toggle-cls @menu-open?]
        [:h1.logo
          [:a {:href "/"}
            [:img {:src "/static/img/logo.png", :alt "Cheesecake FS Challenge"}]]]
        [:nav {:class (str "navigation clearfix " toggle-cls)}
          [:ul.pull-right
            (for [subj subjects
                  :let [name (:name subj)
                        path (str "/subject/" (:id subj))]] 
              [:li
                [:a {:href path, :title name} name]])
            [:li.login-btn
              (if (logged-in?)
                [:a {:href "/me", :title "Profile"} "My Profile"]
                [:a {:href "/login", :title "Login"} "Login"])]]]]]))  

(defn single-article [{:keys [hero_img title text meta-data author subject]} grid-cls]
  (let [article-cls (str "post " (get meta-data :headline ""))
        grid-cls    (if (contains? meta-data :headline)
                      "grid__item sm-w-1/1 md-w-1/2"
                      grid-cls)]
    [:div {:class grid-cls}
      [:article {:class article-cls}
        [:ul.subjects
          [:li {:class (subject :color)} (subject :name)]]
        [:div.content
          (when (contains? meta-data :highlighted)
            [:div.article-heroimg
              [:img.img-responsive {:src hero_img}]])
          [:h2.title title]
          [:aside.byline.clearfix
            [:img.author-img.pull-left {:src "/static/img/author.jpg"}]
            [:span (str "by " (author :name))]]
          (when-not (contains? meta-data :headline)  
            [:p.text text])]]]))

(defn render-highlighted [articles]
  (let [highlighted (highlight (take 3 articles))]
    [:section.highlighted
      [:div.grid
          (for [article highlighted]
              [single-article article "grid__item sm-w-1/2 md-w-1/4"])]]))

(defn render-remaining [articles]
  (let [remaining (drop 3 articles)]
    [:section
      [:div.grid
        (for [article remaining]
            [single-article article "grid__item sm-w-1/2 md-w-1/3"])]]))

(defn select-subjects [subjects sel-subjects]
  [:div
    [:ul.pills
      (for [subj subjects
            :let [name       (:name subj)
                  selected?  (boolean (some #{subj} sel-subjects))
                  selected   (if selected? "-selected" "") 
                  pill-color (str " -pill-" (subj :color))
                  el-classes (str selected pill-color)]]
        [:li
          [:a {:class el-classes, :on-click #(dispatch [:select-subject subj])}
            name]])]
    [:div.actions
      [:button.btn.btn--primary.h-align {:on-click #(dispatch [:save-subjects])} "Save"]]
    [:div.actions
      [:a.back.h-align {:href "/"} "Back to Home"]]])
