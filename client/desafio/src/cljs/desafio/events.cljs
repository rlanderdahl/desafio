(ns desafio.events
    (:require [ajax.core :as ajax]
              [re-frame.core :refer [reg-event-db reg-event-fx reg-fx]]
              [accountant.core :as accountant]
              [desafio.db :as db]))

;; -- Effectful Handlers ----------------------------------------------------------

(reg-fx
 :set-page
 (fn [page]
    (accountant/navigate! page)))


;; -- Effectful Event Handlers ----------------------------------------------------------
 
;; on app startup, create initial state
;; usage:  (dispatch [:initialise-db])  
(reg-event-fx
  :initialize-db
  (fn  
    [{db :db} _]
    {:dispatch [:initialize-articles]
     :db (assoc db/default-db :loading? true)}))

;; on app startup, grab articles from backend db
;; usage:  (dispatch [:initialize-articles])
(reg-event-fx
  :initialize-articles
  (fn [_ _]
    {:http-xhrio {:method          :get
                  :uri             "/api/articles/"
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success      [:initialize-articles-ok]}
     :dispatch [:initialize-subjects]}))

;; on app startup, grab subjects from backend db
;; usage:  (dispatch [:initialize-subjects])
(reg-event-fx
  :initialize-subjects
  (fn [_ _]
     {:http-xhrio {:method          :get
                   :uri             "/api/subjects/"
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:initialize-subjects-ok]}}))

;; handle ajax post with user credentials, obtain auth token and
;; pass it on to :login-set-token
;; usage:  (dispatch [:login-submit])
(reg-event-fx
 :login-submit
 (fn [{:keys [db]} _]
   {:db         db
    :http-xhrio {:method          :post
                 :uri             "/obtain-auth-token/"
                 :params          (:login-data db)
                 :format          (ajax/url-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [:login-set-token]
                 :on-error        [:login-set-error]}}))

;; handle the actual token storage and redirect user to his panel
;; usage:  (dispatch [:login-set-token])
(reg-event-fx
 :login-set-token
 (fn [{:keys [db]} [_ resp]]
   {:db       (-> db
                  (assoc :token (:token resp))
                  (assoc-in [:login-errors :general] []))
    :set-page "/me"}))

;; store user-selected subjects (copy data from :sel-subjects)
;; usage:  (dispatch [:save-subjects])
(reg-event-fx
  :save-subjects
  (fn [{:keys [db]} _]
    {:db       (assoc-in db [:svd-subjects] (db :sel-subjects))
     :set-page "/"}))

;; initialize user dashboard
;; usage:  (dispatch [:load-me-panel])
(reg-event-fx
  :load-me-panel
  (fn  
    [{db :db} _]
    {:dispatch [:set-active-panel :me-panel]
     :db (assoc-in db [:sel-subjects] (db :svd-subjects))}))


;; -- Pure Event Handlers ----------------------------------------------------------

;; store (client-side) articles that were grabbed on app startup
;; usage:  (dispatch [:initialize-articles-ok])
(reg-event-db
  :initialize-articles-ok
  (fn [db [_ {articles :results} response]]
    (assoc db :articles articles)))

;; store (client-side) subjects that were grabbed on app startup
;; usage:  (dispatch [:initialize-subjects-ok])
(reg-event-db
  :initialize-subjects-ok
  (fn [db [_ {subjects :results} response]]
    (assoc db :subjects subjects)))

;; store user credentials as they're being typed
;; usage:  (dispatch [:login-set-attr keys value])
(reg-event-db
 :login-set-attr
 (fn [db [_ keys value]]
   (assoc-in db (apply vector :login-data keys) value)))

;; store login errors
;; usage:  (dispatch [:login-set-error error-message])
(reg-event-db
 :login-set-error
 (fn [db [_ err]]
   (assoc-in db [:login-errors :general] ["User/password combination invalid."])))

;; select/deselect subjects
;; usage:  (dispatch [:select-subject subject])
(reg-event-db
  :select-subject
  (fn [db [_ subject]]
    (if (some #{subject} (db :sel-subjects))
      (update-in db [:sel-subjects] #(remove #{subject} (db :sel-subjects)))
      (update-in db [:sel-subjects] #(conj % subject)))))

;; Toggle menu open/close
;; usage:  (dispatch [:toggle-menu status (true/false)])
(reg-event-db
  :toggle-menu
  (fn [db [_ menu-status]]
    (assoc db :menu-open? menu-status)))

;; navigation handler, switches the panel currently in view
;; usage:  (dispatch [:set-active-panel :panel])
(reg-event-db
 :set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(reg-event-db
  :set-subject-id
  (fn [db [_ id]]
    (assoc db :subject-id id)))
