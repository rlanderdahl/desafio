(ns desafio.core
    (:require [reagent.core :as reagent]
              [re-frame.core :as re-frame]
              [re-frisk.core :refer [enable-re-frisk!]]
              [day8.re-frame.http-fx]
              [desafio.events]
              [desafio.subs]
              [desafio.routes :as routes]
              [desafio.views :as views]
              [desafio.config :as config]))

;; -- Debugging aids ----------------------------------------------------------
;;
(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (enable-re-frisk!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

;; -- Bootstrap the app -------------------------------------------------------------
;; Put an initial value into app-db.
;; The event handler for `:initialise-db` can be found in `events.cljs`
;;
(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [:initialize-db])
  (dev-setup)
  (mount-root))
