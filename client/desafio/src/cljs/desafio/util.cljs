(ns desafio.util
  (:require [re-frame.core :refer [subscribe]]))

;; -- Misc utility functions ----------------------------------------------------------

(defn first? [i seq]
   (= i (first seq)))

(defn on-enter [f]
  (fn [evt]
    (when (= "Enter" (.-key evt))
      (f evt))))

(defn highlight [articles]
  (map (fn [article]
           (if (first? article articles)
             (update-in article [:meta-data] assoc :headline "-main" :highlighted true)
             (update-in article [:meta-data] assoc :highlighted true)))
    articles))

(defn logged-in? []
  @(subscribe [:token]))
