(ns desafio.db)

;; -- App DB ----------------------------------------------------------
;; When the application first starts, this will be the value put in app-db
;;  Look in `core.cljs` for  "(dispatch-sync [:initialise-db])"

(def default-db {:active-panel nil               ;; view which is currently being displayed                
                 :articles     nil                  
                 :subjects     nil               ;; subjects (categories) grabbed from django db
                 :svd-subjects nil               ;; user-saved subjects
                 :sel-subjects []                ;; active subjects being selected by user, not saved yet
                 :token        nil               ;; auth token
                 :menu-open?   false
                 :subject-id   nil
                 :login-data   {:username ""
                                :password ""}
                 :login-errors {:general []}})
