# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from httpproxy.views import HttpProxy

from desafio.core import views as core_views
from desafio.users import views as user_views


router = routers.DefaultRouter()
router.register(r'articles', core_views.ArticleViewSet)
router.register(r'subjects', core_views.SubjectViewSet)
router.register(r'users', user_views.UserViewSet)


urlpatterns = [
    url(settings.ADMIN_URL, admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^obtain-auth-token/$', obtain_auth_token),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name="home"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        # proxy for client development, avoids CORS problems
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
        # url(r'^(?P<url>.*)$', HttpProxy.as_view(base_url='http://localhost:3449')),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]
