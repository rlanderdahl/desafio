# Latest Django
django==1.10.6

# Configuration
django-environ==0.4.1
whitenoise==3.3.0

# Forms
django-braces==1.11.0
django-crispy-forms==1.6.1

# Models
django-autoslug==1.9.3
django-model-utils==2.6.1
awesome-slugify==1.6.5

# API
djangorestframework==3.6.2

# Images
Pillow==4.0.0

# Password storage
argon2-cffi==16.3.0

# For user registration, either via email or social
# Well-built with regular release cycles!
django-allauth==0.31.0


# Python-PostgreSQL Database Adapter
psycopg2==2.7

# Time zones support
pytz==2016.10
